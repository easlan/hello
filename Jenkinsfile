node {
    def app

    stage('Clone repository') {
        /* clone the repository to our workspace */
        checkout scm
    }

    stage('Build image') {
        /* This builds the actual image; synonymous to
         * docker build on the command line */
        sh './gradlew clean build'
        app = docker.build("eraya/kartaca-hello")
    }

    stage('Run tests') {
        /* Ideally, we would run a test framework against our image */
        app.inside {
            sh 'echo "Tests passed"'
        }
    }

    stage('Push image') {
        /* Push the image with two tags:
         * First, the incremental build number from Jenkins
         * Second, the 'latest' tag.
         * Pushing multiple tags is cheap, as all the layers are reused. */
        docker.withRegistry('https://registry.hub.docker.com', 'docker-hub-credentials') {
            app.push("${env.BUILD_NUMBER}")
            app.push("latest")
        }
    }

    stage('Provision test env') {
        /* up the test environment */
        build job: 'Provision Consul Infra on AWS'
    }

    stage('Deploy to test env') {
        build job: 'Deploy_Hello_to_Test'
    }

    stage('Integration tests') {
        /* Run tests */
        echo "Running tests"
    }

    stage('Destroy test env') {
        /* build job: 'Destroy Infra' */
        echo "NOT destroying the test environment"
    }
}
