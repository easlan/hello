#!/bin/sh

# Deploys the hello app to worker nodes

set -e

vm="tf-manager1"

ec2_public_ip=$(aws ec2 describe-instances \
  --filters Name="tag:Name,Values=${vm}" \
  --output text --query 'Reservations[*].Instances[*].PublicIpAddress')

ssh -oStrictHostKeyChecking=no -T \
  -i ~/.ssh/kartacatest.pem ubuntu@${ec2_public_ip} << EOSSH
  docker service create \
    --name kartaca-hello \
    --publish 80:8080/tcp \
    --constraint node.role==worker \
    --replicas 3 \
    eraya/kartaca-hello:latest
EOSSH

echo "Script completed..."
